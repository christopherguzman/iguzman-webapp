import React from 'react';
import classNames from 'classnames';

export const Arrows = (props) => {
  return(
    <>
      <div className={classNames('swiper-button swiper-button-prev', {
        'swiper-button-hide': !props.display
      })} style={{backgroundColor: props.theme.primary_color }}>
        <i className="left"></i>
      </div>
      <div className={classNames('swiper-button swiper-button-next', {
        'swiper-button-hide': !props.display
      })} style={{backgroundColor: props.theme.primary_color }}>
        <i className="right"></i>
      </div>
    </>
  );
}
