import React from 'react';

export const Pagination = ( props ) => {
  return (
    <div 
      className='swiper-pagination'
      style={{ display: props.display ? 'block' : 'none' }}>
    </div>
  );
}
