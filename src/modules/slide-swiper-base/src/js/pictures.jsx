import React from 'react';

export const Picture = (props) => {
  return (
    <div className='swiper-wrapper'>
      {
        props.pictures.map((i, index) =>
          <div 
            className='swiper-slide'
            style={{ backgroundImage: `url(${i.attributes.img_picture})`}}
            key={index}
          ></div>
        )
      }
    </div>
  );
}
