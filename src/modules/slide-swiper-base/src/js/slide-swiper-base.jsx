import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import Swiper from 'swiper';
import { Arrows } from './arrows';
import { Picture } from './pictures';
import { Pagination } from './pagination';

export default class SlideSwiper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0
    }
    this.swiper;
    this.titleContainer = React.createRef();
    this.titleWrapper = React.createRef();
    this.pictures;
  }
  setCardHeight = () => {
    this.titleWrapper.current.style.height = `${this.titleContainer.current.offsetHeight}px`;
  }
  onHomePageSlideChange = () => {
    this.setState({currentSlide: this.swiper.realIndex});
    this.setCardHeight();
  }
  getTitle = () => {
    const current =  this.state.currentSlide;
    const pictures = this.pictures;
    const slide = pictures[current].attributes;
    const language = this.props.config.language;
    if ( slide[`${language}_description`] ) {
      return slide[`${language}_description`];
    } else if ( slide[`${language}_title`] ) {
      return slide[`${language}_title`];
    }
    return null;
  }
  componentDidMount() {
    window.document.addEventListener('readystatechange', (event) => {
      if ( window.document.readyState === 'complete') {
        const slide = {...this.props.slide};
        this.swiper = new Swiper('.swiper-container', {
          direction: 'horizontal',
          loop: true,
          pagination: {
            el: '.swiper-pagination'
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          },
          autoplay: { delay: slide.attributes.delay }
        });
        this.swiper.on('slideChange', this.onHomePageSlideChange.bind(this));
        window.addEventListener('resize', this.setCardHeight);
        this.setCardHeight();
      }
    });
  }
  render() {
    const slide = {...this.props.slide};
    const theme = {...this.props.theme};
    this.pictures = slide.relationships.pictures.data;
    this.pictures.sort((a, b) => {
      return a.attributes.order - b.attributes.order;
    });
    const href = this.pictures[this.state.currentSlide].attributes.href;
    return (
      <div className='swiper-container'>
        <Picture pictures={this.pictures}/>
        <i className='triangle'
          style={{display: slide.attributes.triangle ? 'block' : 'none'}}></i>
        <a className='slides-horizontal'
          ref={this.titleWrapper}
          href={href}
          style={{display: (this.getTitle() && slide.attributes.captions) ? 'block' : 'none'}}>
          <div ref={this.titleContainer}>
            { ReactHtmlParser(this.getTitle()) }
          </div>
        </a>
        <Arrows display={slide.attributes.navigation_arrows} theme={theme}/>
        <Pagination display={slide.attributes.pagination} theme={theme}/>
      </div>
    );
  }
}
