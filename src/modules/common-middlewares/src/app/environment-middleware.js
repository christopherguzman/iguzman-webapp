// Not for production
export const notForProduction = function( req, res, next ) {
  const config = req.context.config;
  const production = config.env.production;
  if ( production ) {
    return res.redirect('/');
  }
  return next();
};

// Only in production
export const onlyForProduction = function( req, res, next ) {
  const config = req.context.config;
  const production = config.env.production;
  if ( !production ) {
    return res.redirect('/');
  }
  return next();
};
