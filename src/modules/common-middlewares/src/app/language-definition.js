export const validateLanguaje = (lang, page, res) => {
  const english = page.data.attributes.english_available;
  const spanish = page.data.attributes.spanish_available;
  if ( lang !== 'es' && lang !== 'en' ) {
    res.writeHead(302, { 'Location': '/en/' });
    return res.end();
  }
  if ( !english || !spanish ) {
    if ( english && lang === 'es' ) {
      res.writeHead(302, { 'Location': '/en/' });
      return res.end();
    }
    if ( spanish && lang === 'en' ) {
      res.writeHead(302, { 'Location': '/es/' });
      return res.end();
    }
  }
  return false;
}

export const lenguageDefinition = (req, res, page) => {
  let browserLang = req.headers["accept-language"].split('-')[0];
  const english = page.data.attributes.english_available;
  const spanish = page.data.attributes.spanish_available;
  if ( !english || !spanish ) {
    if ( english ) {
      res.writeHead(302, { 'Location': '/en/' });
      return res.end();
    }
    res.writeHead(302, { 'Location': '/es/' });
    return res.end();
  }
  if ( browserLang !== 'es' && browserLang !== 'en' ) {
    browserLang = 'en';
  }
  if ( browserLang === 'en' && english ) {
    res.writeHead(302, { 'Location': '/en/' });
    return res.end();
  }
  res.writeHead(302, { 'Location': '/es/' });
  return res.end();
}