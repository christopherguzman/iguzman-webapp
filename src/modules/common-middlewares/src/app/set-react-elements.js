import React from 'react';
import { renderToString } from "react-dom/server";

import NavBar from 'modules/nav-bar-base/src/js/nav-bar-base';
import SlideSwiper from 'modules/slide-swiper-base/src/js/slide-swiper-base';
import InfoGridBase from 'modules/info-grid-base/src/js/info-grid-base';
import BannerBigPicture from 'modules/banner-big-picture/src/js/banner-big-picture';

/**
 * Set React Elements middleware
 * @param {Object} req 
 * @param {Object} res 
 * @param {Function} next 
 */
export const setReactElements = function( req, res, next ) {
  let content = '';
  const config = req.context.config;
  const page = req.page;
  const theme = page.data.attributes.theme;
  const sections = page.data.relationships.sections.data;
  sections.sort((a, b) => {
    return a.attributes.order - b.attributes.order;
  });
  let currentSection;
  sections.forEach(i => {
    const href = i.attributes.href ? i.attributes.href : '';
    const expectedSection = `/${config.language}/${href}`;
    let url = req.url;
    if ( url[url.lenght-1] !== '/' ) {
      url = `${url}/`;
    }
    if ( url === expectedSection || url === `${expectedSection}/` ) {
      const modules = i.relationships.modules.data;
      modules.sort((a, b) => {
        return a.attributes.order - b.attributes.order;
      });
      modules.forEach(j => {
        if ( j.relationships.nav_bar.data ) {
          const cont = j.relationships.nav_bar.data;
          const navBar = renderToString(<NavBar config={config} page={page}/>);
          content += `<section id='${cont.attributes.name}'>${navBar}</section>`;
        }
        if ( j.relationships.slide.data ) {
          const cont = j.relationships.slide.data;
          const slide = renderToString(<SlideSwiper config={config} slide={cont} theme={theme}/>);
          content += `<section id='${cont.attributes.name}'>${slide}</section>`;
        }
        if ( j.relationships.info_grid.data ) {
          const cont = j.relationships.info_grid.data;
          const infoGrid = renderToString(<InfoGridBase config={config} theme={theme} data={cont}/>);
          content += `<section id='${cont.attributes.name}'>${infoGrid}</section>`;
        }
        if ( j.relationships.banner.data ) {
          const cont = j.relationships.banner.data;
          const Banner = renderToString(<BannerBigPicture config={config} theme={theme} data={cont}/>);
          content += `<section id='${cont.attributes.name}'>${Banner}</section>`;
        }
      });
      currentSection = i;
    }
  });
  req.page.data.relationships.sections.data = sections;
  req.page.data.relationships.currentSection = {
    data: currentSection
  };
  req.context.content = content;
  return next();
};
