/**
 * Set Context middleware
 * @param {Object} req 
 * @param {Object} res 
 * @param {Function} next 
 */
export const setContext = function( req, res, next ) {
  req.context.page = req.page;
  req.context.jsPage = JSON.stringify(req.page);
  req.context.jsConfig = JSON.stringify(req.context.config);
  return next();
};
