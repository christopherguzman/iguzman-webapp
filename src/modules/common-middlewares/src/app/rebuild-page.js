import { rebuildModel } from 'modules/tool-box/src/js/tool-box';
import Cache from 'modules/cache-handler/src/app/cache-handler';

/**
 * Set React Elements middleware
 * @param {Object} req 
 * @param {Object} res 
 * @param {Function} next 
 */
export const rebuildPage = function( req, res, next ) {
  if ( req.page ) {
    return next();
  }
  let host = `http://${req.headers.host}`;
  if ( req.connection.encrypted ) {
    host = `https://${req.headers.host}`
  }
  let page = req.raw_page;
  let included = page.included;
  // Deleting User field
  delete page.data.relationships.user;
  included.forEach(i => {
    if ( i.type === 'Module' ) {
      delete i.relationships.section;
    }
    if ( i.type === 'SlidePictures' ) {
      delete i.relationships;
    }
    if ( i.type === 'InfoGridItem' ) {
      delete i.relationships;
    }
  });
  page.data = rebuildModel(page.data, included);
  // Setting theme
  page.data.attributes.theme = {
    primary_color: page.data.attributes.primary_color,
    primary_light_1_color: page.data.attributes.primary_light_1_color,
    primary_light_2_color: page.data.attributes.primary_light_2_color,
    secondary_color: page.data.attributes.secondary_color,
    secondary_light_1_color: page.data.attributes.secondary_light_1_color,
    secondary_light_2_color: page.data.attributes.secondary_light_2_color,
  }
  // Favicon
  if ( !page.data.attributes.img_favicon ) {
    page.data.attributes.img_favicon = page.data.attributes.img_logo;
  }
  // Setting Social Media
  const socialMedia = [];
  if ( page.data.attributes.whatsapp ) {
    const state = page.data.relationships.address.data.relationships.region.data.relationships.state.data;
    const country = state.relationships.country.data.attributes;        
    socialMedia.push({
      name: 'WhatsApp',
      icon:'icofont-whatsapp',
      prefix: 'https://api.whatsapp.com/send?phone=',
      suffix: '&text=Hola%20estoy%20interesado%20en%20sus%20servicios',
      href: `+${country.phone_code}1${page.data.attributes.whatsapp}`
    });
  }
  if ( page.data.attributes.facebook ) {
    socialMedia.push({
      name: 'Facebook',
      icon:'icofont-facebook',
      href: page.data.attributes.facebook
    });
  }
  if ( page.data.attributes.twitter ) {
    socialMedia.push({
      name: 'Twitter',
      icon:'icofont-twitter',
      href: page.data.attributes.twitter
    });
  }
  if ( page.data.attributes.youtube ) {
    socialMedia.push({
      name: 'YouTube',
      icon:'icofont-youtube',
      href: page.data.attributes.youtube
    });
  }
  page.data.attributes['social-media'] = socialMedia;
  // Saving in cache
  Cache.save( 'pages', host, page );
  req.page = page;
  return next();
};
