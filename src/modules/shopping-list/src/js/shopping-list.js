import React from 'react';
import { render } from 'react-dom';
import ShoppingList from './components/shopping-list';

const shoppingList = document.getElementById('shopping-list');

if ( shoppingList ) {
  render( <ShoppingList name='Christopher Guzman' />, shoppingList );
}
