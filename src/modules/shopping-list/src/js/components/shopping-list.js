import React, { Component } from 'react';

export default class ShoppingList extends Component {

  state = {
    foo: null
  };

  changeFoo = () => {
    this.setState({ foo: "Bar"});
  }

  renderElement = () => {
    if ( this.state.foo ) {
      return (
        <li>{this.state.foo}</li>
      );
    }
    return null;
  }

  render() {
    return (
      <div className="shopping-list">
        <h3 onClick={() => this.changeFoo()}>
          Shopping List for {this.props.name}
        </h3>
        <ul>
          {this.renderElement()}
          <li>Instagram</li>
          <li>WhatsApp</li>
          <li>Add something</li>
        </ul>
      </div>
    );
  }
}
