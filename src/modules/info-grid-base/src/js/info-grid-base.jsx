import React from 'react';
import Title from 'modules/tool-box/src/js/title';
import SubTitle from 'modules/tool-box/src/js/sub-title';
import InfoGridItem from './item';

export default (props) => {
  const language = props.config.language;
  const data = props.data.attributes;
  const theme = props.theme;
  const items = props.data.relationships.items.data;
  items.sort((a, b) => {
    return a.attributes.order - b.attributes.order;
  });

  return (
    <div className='section InfoGrid'>
      <div className='InfoGrid__header'>
        <Title title={data[`${language}_title`]}
          cssClass='InfoGrid' cssStyle={{color: theme.primary_color}} />
        {
          data[`${language}_sub_title`] ? 
            <SubTitle subTitle={data[`${language}_sub_title`]} cssClass='InfoGrid' /> : 
            null
        }
      </div>
      {
        items.map((i, index) =>
          {
            return (<InfoGridItem
              language={language}
              data={i}
              key={index}
            />)
          }
        )
      }
    </div>
  );
}
