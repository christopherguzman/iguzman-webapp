import React, { useState, useEffect } from 'react';
import ReactHtmlParser from 'react-html-parser';

export default (props) => {
  const data = props.data.attributes;
  
  return (
    <div className='InfoGrid__item'>
      <div className='InfoGrid__icon'>
        <i style={{color: data.color}} className={data.icon}></i>
      </div>
      <span className='InfoGrid__item-title'>{ data[`${props.language}_title`] }</span>
      <div className='InfoGrid__item-description'>
        { ReactHtmlParser(data[`${props.language}_description`]) }
      </div>
    </div>
  );
}
