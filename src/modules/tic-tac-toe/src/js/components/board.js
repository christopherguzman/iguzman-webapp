import React, { Component } from 'react';
import Square from './square';
import CalculateWinner from './calculateWinner';

export default class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      squares: Array(9).fill(null),
      xIsNext: true
    };
  }

  handleClick(i) {
    const squares = this.state.squares.slice();
    if ( CalculateWinner(squares) || squares[i] ) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      squares: squares,
      xIsNext: !this.state.xIsNext
    });
  }

  returnSquare(i) {
    return (
      <Square 
        value={this.state.squares[i]} 
        onClick={() => this.handleClick(i)}
      />
    );
  }

  render() {
    const winner = CalculateWinner(this.state.squares);
    let status;
    if ( winner ) {
      status = `Winner is ${winner}`;
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }
    
    return (
      <>
        <div className='game__board-status'>{status}</div>
        <div className='game__board-row'>
          {this.returnSquare(0)}
          {this.returnSquare(1)}
          {this.returnSquare(2)}
        </div>
        <div className='game__board-row'>
          {this.returnSquare(3)}
          {this.returnSquare(4)}
          {this.returnSquare(5)}
        </div>
        <div className='game__board-row'>
          {this.returnSquare(6)}
          {this.returnSquare(7)}
          {this.returnSquare(8)}
        </div>
      </>
    );
  }
}
