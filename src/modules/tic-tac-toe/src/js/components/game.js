import React, { Component } from 'react';
import { render } from 'react-dom';
import Board from './board';

export default class Game extends Component {
  render() {
    return (
      <div className='game'>
        <div className='game__board'>
          <Board />
        </div>
        <div className='game__board-info'>
          <div className='game__board-info--status'></div>
          <ol></ol>
        </div>
      </div>
    );
  }
}
