import React from 'react';
import { hydrate } from 'react-dom';
import Game from './components/game';

const container = document.getElementById('tic-tac-toe');

if ( container ) {
  hydrate( <Game/>, container );
}
