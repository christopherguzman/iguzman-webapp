/**
 * Cache Handler 
 */

class Cache {
  cache;

  constructor() {
    this.cache = {};
  }

  save = ( entity, key, object ) => {
    if ( !this.cache[entity] ) {
      this.cache[entity] = {};
    }
    this.cache[entity][key] = object;
  }

  get = ( entity, key ) => {
    if ( !this.cache[entity] ) {
      this.cache[entity] = {};
    }
    return this.cache[entity][key] || null;
  }
}

const instance = new Cache();
Object.freeze(instance);

export default instance;
