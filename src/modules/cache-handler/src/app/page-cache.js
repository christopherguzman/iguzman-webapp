import axios from 'axios';
import Cache from './cache-handler';
import {
  validateLanguaje
} from 'modules/common-middlewares/src/app/language-definition'
let config = require('../../../../config/local.json');
if ( process.env.NODE_ENV ) {
  config = require(`../../../../config/${process.env.NODE_ENV.trim()}.json`);
}

/*
 *  Verifying Cache Page Middleware
 */
export const verifyCachePage = ( req, res, next ) => {
  let host = `http://${req.headers.host}`;
  if ( req.connection.encrypted ) {
    host = `https://${req.headers.host}`
  }
  const cachedPage = Cache.get( 'pages', host );
  if ( cachedPage === null ) {
    return next();
  }
  let q = `${config.env.api_base}pages/?filter[dns]=${host}`;
  q += '&fields[Page]=version,spanish_available,english_available';
  axios.get(q)
  .then(function (response) {
    const page = response.data;
    if ( !page.data.length ) {
      return res.status(404).json({error: 'Not found'});
    }
    page.data = page.data[0];
    const version = page.data.attributes.version;
    const cachedVersion = cachedPage.data.attributes.version;
    let requestLang = req.context.config.language;
    if ( version === cachedVersion ) {
      req.page = cachedPage;
    }
    return validateLanguaje(requestLang, page, res) || next();
  })
  .catch(function (error) {
    return res.json(error);
  });
};

/*
 *  Page Middleware
 */
export const getPage = ( req, res, next ) => {
  if ( req.page ) {
    return next();
  }
  let host = `http://${req.headers.host}`;
  if ( req.connection.encrypted ) {
    host = `https://${req.headers.host}`
  }
  let q = `${config.env.api_base}pages/?filter[dns]=${host}`;
  q += '&include=font,address,branches';
  q += ',address.region,address.region.state,address.region.state.country';
  q += ',sections,sections.modules,sections.modules.nav_bar';
  q += ',sections.modules.slide,sections.modules.slide.pictures';
  q += ',sections.modules.info_grid,sections.modules.info_grid.items';
  q += ',sections.modules.banner';
  axios.get(q)
  .then(function (response) {
    const page = response.data;
    if ( !page.data.length ) {
      return res.status(404).json({error: 'Not found'});
    }
    page.data = page.data[0];
    let requestLang = req.context.config.language;
    req.raw_page = page;
    return validateLanguaje(requestLang, page, res) || next();
  })
  .catch((err) => {
    return res.json(err);
  });
};
