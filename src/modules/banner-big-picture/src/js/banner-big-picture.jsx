import React, { useState, useEffect } from 'react';
import ReactHtmlParser from 'react-html-parser';
import Title from 'modules/tool-box/src/js/title';
import SubTitle from 'modules/tool-box/src/js/sub-title';

export const getContent = (props) => {
  const language = props.config.language;
  const data = props.data.attributes;
  const theme = props.theme;
  return (
    <>
      <Title title={data[`${language}_title`]}
          cssClass='BannerBigPicture'
          cssStyle={{color: data.font_color}} />
      <SubTitle subTitle={data[`${language}_sub_title`]}
          cssClass='BannerBigPicture'
          cssStyle={{color: data.font_color}} />
      <div className='BannerBigPicture__description'
        style={{color: data.font_color}}>
        { ReactHtmlParser(data[`${language}_description`]) }
      </div>
    </>
  );
};

export default (props) => {
  let [count, setCount] = useState(false);

  const data = props.data.attributes;
  const gradient = `linear-gradient(19deg, ${data.color_1}, ${data.color_2})`;

  return (
    <div className='BannerBigPicture'>
      <div className='BannerBigPicture__picture'
        style={{backgroundImage: `url(${data.img_picture})`}}></div>
      <div className='BannerBigPicture__gradient'
        style={{backgroundImage: gradient}}>
        {getContent(props)}
        {
          count ? 'Activo' : 'False'
        }
        <div className="switch">
          <label>
            Off
            <input type="checkbox" value={count} onChange={e => setCount(e.target.checked)} />
            <span className="lever"></span>
            On
          </label>
        </div>
      </div>
      <div className='BannerBigPicture__wrapper'></div>
    </div>
  );
}
