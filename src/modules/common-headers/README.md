# Common Headers

Simple NodeJS package to build deacouple and plug and play modules for web apps. Some features:

  - Work alone modules
  - Deacouple
  - Plug and play
  - Fixtures

### Tech

Frameworks and tools used :

* [Vue] - Frontend framework
* [Bower] - Frontend library manager
* [ESLint] - Linter for Javascript
* [StyleLint] - Linter for SASS
* [Nodemon] - Server runner
* [Webpack] - Bundle to build FrontEnd assets
* [Mocha] - Test Framework
* [Chai] - Test Library
* [SASS] - CSS Framework
* [Express.js] - NodeJS Backend Framework
* [Babel] - JavaScript compiler and configurable transpiler

### Installation

NodeJS Module requires [Node.js](https://nodejs.org/) v10.13.0+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ npm i
```

For production environments...

```sh
$ npm i --production
```

### To Run

To build module

```sh
$ npm run build
```

To run server

```sh
$ npm run start
```

To run tests

```sh
$ npm run test
```

To check coverage

```sh
$ npm run coverage
```

To check linter

```sh
$ npm run linter:js
$ npm run linter:scss
```

### Todos

 - Write MORE Tests
 - Add Night Mode
 - Add Single Page demo
 - Improve this README.md

License
----

MIT

**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[//]: # (Marckdown guide -> https://dillinger.io/)

[//]: # (ES6 Node -> https://dev.to/bnorbertjs/my-nodejs-setup-mocha--chai-babel7-es6-43ei)

  [link]: <https://my-link.com>