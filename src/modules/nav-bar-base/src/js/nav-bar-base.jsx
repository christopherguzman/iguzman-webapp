import React from 'react';
import NavbarTop from './components/top-nav-bar';
import SideNavBar from './components/side-nav-bar';

export default ( props ) => {
  const config = props.config;
  const lang = config.language;

  const page = props.page;
  const name = page.data.attributes.name;
  const logo = page.data.attributes.img_logo;
  const theme = page.data.attributes.theme;
  const social = page.data.attributes['social-media'];
  const logging = page.data.attributes.loggin_available;
  const spa = page.data.attributes.spanish_available;
  const eng = page.data.attributes.english_available;

  const menu = page.data.relationships.sections.data;

  const currentSection = {...page.data.relationships.currentSection};
  const cs = {...currentSection.data};

  return (
    <>
      <NavbarTop
        lang={lang}
        name={name}
        logo={logo}
        theme={theme}
        social={social}
        logging={logging}
        spa={spa}
        eng={eng}
        menu={menu}
        cs={cs.attributes}
        type='nav' />
      <SideNavBar
        lang={lang}
        name={name}
        logo={logo}
        theme={theme}
        social={social}
        logging={logging}
        spa={spa}
        eng={eng}
        menu={menu}
        cs={cs.attributes}
        type='side-nav' />
    </>
    /* <>
      {/* <TopNavBar
        language={config.language}
        menu={menu} 
        theme={page.data.attributes.theme}
        logo={page.data.attributes.img_logo}
        social={page.data.attributes['social-media']} 
        openSideBar={() => this.changeSideBarState()}
        logginAvailable={page.data.attributes.loggin_available}
        spanishAvailable={page.data.attributes.spanish_available}
        englishAvailable={page.data.attributes.english_available}
      />
      <SideNavBar 
        language={config.language}
        menu={menu} 
        theme={page.data.attributes.theme}
        logo={page.data.attributes.img_logo}
        social={page.data.attributes['social-media']} 
        isOpenSideNavBar={this.state.isOpenSideNavBar}
        logginAvailable={page.data.attributes.loggin_available}
        spanishAvailable={page.data.attributes.spanish_available}
        englishAvailable={page.data.attributes.english_available}
        closeSideBar={() => this.changeSideBarState()}
      /> */
  );
  // }
}
