import React from 'react';
import classNames from 'classnames';

export default (props) => {
  const social = props.social;
  const type = props.type;

  return (
    <ul className={classNames(`${type}-social-media`, {
      'right hide-on-small-and-down': type === 'nav',
      'left hide-on-med-and-up': type === 'side-nav'
    })}>
      {
        social ? 
        social.map((item, index) => {
          return (
            <li key={index}>
              <a href={
                  (item.prefix ? item.prefix : '') + 
                  item.href +
                  (item.suffix ? item.suffix : '')
                } target='_blank' rel='noreferrer' 
                className={classNames(`${type}-social-media__item`, {
                  'waves-effect': true
                })}>
                <i className={item.icon}></i>
              </a>
            </li>
          );
        }) :
        null
      }
    </ul>
  );
}
