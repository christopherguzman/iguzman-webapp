import React from 'react';
import Logo from './nav-bar-logo';
import NavBarMenu from './nav-bar-menu';
import NavBarSocialMedia from './nav-bar-social-media';
import NavBarAccount from './nav-bar-account';

export default ( props ) => {
  const lang = props.lang;
  const name = props.name;
  const logo = props.logo;
  const theme = props.theme;
  const social = props.social;
  const spa = props.spa;
  const eng = props.eng;
  const log = props.logging;
  const menu = props.menu;
  const cs = {...props.cs};
  const type = props.type;

  return (
    <div id="slide-out" className="sidenav">
      <Logo lang={lang} name={name} logo={logo} type={type}/>
      <NavBarMenu lang={lang} theme={theme} menu={menu} cs={cs} type={type}/>
      <NavBarAccount lang={lang} theme={theme} spa={spa} eng={eng} log={log} type={type}/>
      <NavBarSocialMedia social={social} type={type}/>
      {/* <li>
        <Logo lang={lang} name={name} logo={logo} />
        <div className="user-view">
        <div className="background">
          <img src="images/office.jpg" />
        </div>
        <a href="#user"><img className="circle" src="images/yuna.jpg" /></a>
        <a href="#name"><span className="white-text name">John Doe</span></a>
        <a href="#email"><span className="white-text email">jdandturk@gmail.com</span></a>
      </div>
      </li> */}
      {/* <li><a href="#!"><i className="material-icons">cloud</i>First Link With Icon</a></li>
      <li><a href="#!">Second Link</a></li>
      <li><div className="divider"></div></li>
      <li><a className="subheader">Subheader</a></li>
      <li><a className="waves-effect" href="#!">Third Link With Waves</a></li> */}
    </div>
  );
  /*constructor(props) {
    super(props);
    this.state = {
      isOpen: props.isOpenSideNavBar
    };
  }
  render() {
    return(
      <>
        <div 
          className={classNames('side-nav__close-button__large', {
            'side-nav__close-button__large--opened': this.props.isOpenSideNavBar,
            'side-nav__close-button__large--closed': !this.props.isOpenSideNavBar,
          })}
          onClick={() => this.props.closeSideBar()}></div>
        <div className={classNames('side-nav', {
            'side-nav--opened': this.props.isOpenSideNavBar,
            'side-nav--closed': !this.props.isOpenSideNavBar,
          })}>
          <a 
            className='side-nav__close-button__small'
            onClick={() => this.props.closeSideBar()}
          ><i className="icofont-close-line"></i></a>
          <NavBarLogo logo={this.props.logo}/>
          <NavBarMenu
            language={this.props.language}
            menu={this.props.menu}
            theme={this.props.theme}
            sideMenu={true}
            closeSideBar={() => this.props.closeSideBar()}
          />
          <NavBarAccount 
            language={this.props.language}
            theme={this.props.theme}
            user={null}
            sideMenu={true}
            logginAvailable={this.props.logginAvailable}
            spanishAvailable={this.props.spanishAvailable}
            englishAvailable={this.props.englishAvailable}
          />
          <NavBarSocialMedia 
            social={this.props.social}
            sideMenu={true}
          />
        </div>
      </>
    );
  }*/
}
