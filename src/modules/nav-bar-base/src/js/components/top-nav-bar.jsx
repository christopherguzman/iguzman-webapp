import React from 'react';
import Logo from './nav-bar-logo';
import NavBarMenu from './nav-bar-menu';
import NavBarSocialMedia from './nav-bar-social-media';
import NavBarAccount from './nav-bar-account';
import HamburguerMenu from './nav-bar-hamb-button';

export default ( props ) => {
  const lang = props.lang;
  const name = props.name;
  const logo = props.logo;
  const theme = props.theme;
  const social = props.social;
  const spa = props.spa;
  const eng = props.eng;
  const log = props.logging;
  const menu = props.menu;
  const cs = {...props.cs};
  const type = props.type;

  return (
    <div className="navbar-fixed">
      <nav className='nav white'>
        <div className='container'>
          <Logo lang={lang} name={name} logo={logo} type={type}/>
          <NavBarMenu lang={lang} theme={theme} menu={menu} cs={cs} type={type}/>
          <NavBarAccount lang={lang} theme={theme} spa={spa} eng={eng} log={log} type={type}/>
          <HamburguerMenu social={social} />
          <NavBarSocialMedia social={social} type={type}/>
        </div>
      </nav>
    </div>
  );

  // constructor(props) {
  //   super(props);
  // }

  // render() {
  //   return (
  //     <nav className={classNames('nav',{})}>
  //       <div className='nav__wrapper'>
  //         <NavBarLogo logo={this.props.logo}/>
  //         <NavBarMenu
  //           language={this.props.language}
  //           menu={this.props.menu}
  //           theme={this.props.theme}
  //         />
  //         <NavBarSocialMedia social={this.props.social}/>
  //         <NavBarAccount 
  //           language={this.props.language}
  //           theme={this.props.theme}
  //           user={null}
  //           logginAvailable={this.props.logginAvailable}
  //           spanishAvailable={this.props.spanishAvailable}
  //           englishAvailable={this.props.englishAvailable}
  //         />
  //         <a 
  //           className='nav__menu-button__mobile'
  //           onClick={() => this.props.openSideBar()}
  //         ><i className="icofont-navigation-menu"></i></a>
  //       </div>
  //     </nav>
  //   );
  // }
}
