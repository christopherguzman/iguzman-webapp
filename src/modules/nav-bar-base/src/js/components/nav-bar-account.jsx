import React from 'react';
import classNames from 'classnames';

export default ( props ) => {
  const lang = props.lang;
  const theme = props.theme;
  const spa = props.spa;
  const eng = props.eng;    
  const log = props.log;
  const user = null;
  const type = props.type;

  const accessLabel = () => {
    if ( user ) {
      return user.name;
    }
    let label = 'Acceder';
    if ( lang == 'en' ) {
      label = 'Access';
    }
    return label;
  };

  return (
    <ul className={classNames(`${type}-account`, {
      'right hide-on-med-and-down': type === 'nav'
    })}>
      {
        ( user || (spa && eng) ) ? <li className={`${type}-account__line`}></li> : null
      }
      <li style={{'display': log ? 'block' : 'none' }}>
        <a href='#'
          style={{'color': user ? theme.primary_color : null}}
          className={`${type}-account__item`}>
          <i className='icofont-ui-user'></i>
          <span>{accessLabel()}</span>
        </a>
      </li>
      <li style={{'display': (spa && eng) ? 'block' : 'none' }}>
        <a href={lang=='es' ? '/en/' : '/es/'}
          style={{'color': theme.primary_color}}
          className={`${type}-account__item waves-effect`}>
          <i className='icofont-world'></i>
          <span>{lang=='es' ? 'EN' : 'ES'}</span>
        </a>
      </li>
    </ul>
  );
}
