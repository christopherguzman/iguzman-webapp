import React, { useState } from 'react';

export default ( props ) => {
  const [over, setOver] = useState(false);

  const lang = props.lang;
  const item = props.item.attributes;
  const theme = props.theme;
  const cs = props.cs;  

  const singlePage = cs.single_page;
  const href = item.href ? item.href : '';
  const csHref = cs.href ? cs.href : '';
  const icon = item.icon;
  const name = item[`${lang}_name`];  

  const overStyle = () => {
    return {
      color: (over || ( href === csHref)) ? theme.primary_color : '#424242'
    };
  };

  return (
    <li>
      <a data-scroll
        href={ !singlePage ? `/${lang}/${href}` : `#${href}` }
        onMouseEnter={() => setOver(true)}
        onMouseLeave={() => setOver(false)}
        className='waves-effect'
        style={overStyle()}>
        {icon ? <i className={icon}></i> : null}
        {name} {over}
      </a>
    </li>
  ); 
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     isOver: false,
  //     over: false
  //   };
  // }
  // componentDidMount() {
  //   const element = this.props.item;
  //   const container = document.getElementById(element.attributes.href);
  //   if ( !container ) {
  //     return;
  //   }
  //   const setItemSelected = this.setItemSelected;
  //   new ScrollWatcher().watch( `#${element.attributes.href}`, -80 )
  //     .on('enter', function(evt) {
  //       return setItemSelected();
  //     })
  // }
  // setItemSelected = () => {
  //   return this.props.setItemSelected(this.props.index);
  // }
  // handleClick = () => {
  //   this.props.setItemSelected(this.props.index);
  //   return this.props.closeSideBar();
  // }
  // render() {
  //   const theme = this.props.theme;
  //   const overStyle = () => {
  //     return {
  //       borderBottom: (this.state.isOver || this.props.item.attributes.over) ? 
  //         `2.5px solid ${theme.primary_color}` : 
  //         '2.5px solid white'
  //     };
  //   };
  //   const singlePage = this.props.item.attributes.single_page;
  //   const language = this.props.language;
  //   let href = this.props.item.attributes.href;
  //   href = href ? href : '';
  //   const icon = this.props.item.attributes.icon;
  //   return (
  //     <li onClick={() => this.handleClick()}>
  //       <a data-scroll
  //         href={
  //           !singlePage ? 
  //           `/${language}/${href}` :
  //           `#${href}`
  //         }
  //         style={overStyle()}
  //         onMouseEnter={() => this.setState({isOver:true})}
  //         onMouseLeave={() => this.setState({isOver:false})}>
  //         {icon ? <i className={icon}></i> : null}
  //         {this.props.item.attributes[`${language}_name`]}
  //       </a>
  //     </li>
  //   ); 
  // }
}
