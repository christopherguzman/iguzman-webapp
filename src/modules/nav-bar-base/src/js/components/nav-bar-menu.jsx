import React from 'react';
import classNames from 'classnames';
import MenuItem from './nav-bar-menu-item';

export default ( props ) => {
  const lang = props.lang;
  const theme = props.theme;
  const menu = props.menu;
  const cs = props.cs;
  const type = props.type;

  return (
    <ul className={classNames(`${type}-menu`, {
      'left hide-on-med-and-down': type === 'nav'
    })}>
      {
        menu.map((i, index) =>
          <MenuItem 
            lang={lang}
            item={i} 
            theme={theme}
            current={false}
            key={index} 
            index={index} 
            cs={cs} />
        )
      }
    </ul>
  );
  /*constructor(props) {
    super(props);
    const menu = [...props.menu];
    menu.forEach(e => {e.attributes.current = false; });
    this.state = {
      menu: menu
    }
  }
  setItemSelected = (index) => {
    const menu = [...this.state.menu];
    menu.forEach(e => {e.attributes.current = false; });
    index >= 0 ? menu[index].attributes.current = true : null;
    this.setState({menu: menu});
  }
  render() {
    return(
      <ul className={classNames('', {
        'nav__menu': !this.props.sideMenu,
        'side-nav__menu': this.props.sideMenu
      })}> 
        {
          this.state.menu.map((element, index) =>
            <MenuItem 
              language={this.props.language}
              item={element} 
              key={index} 
              index={index} 
              theme={this.props.theme}
              closeSideBar={this.props.closeSideBar ? this.props.closeSideBar : () => {}}
              setItemSelected={this.setItemSelected}
            />
          )
        }
      </ul>
    );
  }*/
}
