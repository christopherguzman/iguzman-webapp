import React from 'react';

export default ( props ) => {
  const social = props.social;
  
  return (
    <div className='HamburguerMenu right hide-on-large-only'>
      {
        social.length ? <span className='HamburguerMenu__line left hide-on-small-and-down'></span> : null
      }
      <a href="#" data-target="slide-out"
        className="sidenav-trigger left waves-effect blue-text text-darken-2"
        style={{margin: '0'}}>
        <i className="material-icons">menu</i>
      </a>
    </div>  
  );
}
