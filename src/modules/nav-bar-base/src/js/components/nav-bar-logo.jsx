import React from 'react';

export default ( props ) => {
  const type = props.type;
  
  return (
    <a className={`${type}-logo left waves-effect'`} href={ `/${props.lang}/` }>
      <img src={ props.logo } alt={ props.name }></img>
    </a>
  );
}
