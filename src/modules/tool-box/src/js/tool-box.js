export const getJSONApiURL = ( endpoint, params ) => {
  let config = require('../../../../config/local.json');
  if ( process.env.NODE_ENV ) {
    config = require(`../../../../config/${process.env.NODE_ENV.trim()}.json`);
  }
  let parameters = {
    enabled: true
  };
  for (let i in params) {
    if (params.hasOwnProperty(i)) {
      parameters[i] = params[i];
    }
  }
  let url = `${config.api_base}${endpoint}/?`;
  for (let i in parameters) {
    if (parameters.hasOwnProperty(i)) {
      url += `${i}=${parameters[i]}&`
    }
  }
  return url;
};

const getAttributes = function( included, variable ) {
  if( !variable ) {
    return;
  }
  const id = Number(variable.id);
  const type = variable.type;
  let foreignObject = null;
  if( !id || !type ) {
    return foreignObject;
  }
  for (let i = 0; i < included.length; i++) {
    if( Number(included[i].id) === id && included[i].type === type ) {
      foreignObject = included[i];
      break;
    }
  }
  return foreignObject;
};

export const rebuildModel = ( item, included ) => {
  if ( !item ) {
    return item;
  }  
  let relationships = item.relationships;
  for (let variable in relationships) {
    let innerElement = relationships[variable].data;
    if( !Array.isArray( innerElement ) ) {        
      let foreignObject = getAttributes( included, relationships[variable].data );
      if( foreignObject ) {
        item.relationships[variable].data = foreignObject;
        if ( item.relationships[variable].data.relationships ) {
          item.relationships[variable].data = rebuildModel (
            item.relationships[variable].data, included
          );
        }
      }
    } else {
      let array = relationships[variable].data;
      for (let i = 0; i < array.length; i++) {
        let foreignObject = getAttributes( included, array[i] );
        if( foreignObject ) {
          item.relationships[variable].data[i] = foreignObject;
          if ( item.relationships[variable].data[i].relationships ) {
            item.relationships[variable].data[i] = rebuildModel (
              item.relationships[variable].data[i], included
            );
          }
        }
      }
    }
  }
  return item;
};
