import React from 'react';

export default (props) => {
  const cssClass = `title ${props.cssClass}__title`;
  return (
    <h2 className={cssClass} style={props.cssStyle}>
      {props.title}
    </h2>
  )
}
