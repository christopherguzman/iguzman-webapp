import React from 'react';

export default (props) => {
  const cssClass = `sub-title ${props.cssClass}__sub-title`;
  return (
    <span className={cssClass} style={props.cssStyle}>
      {props.subTitle}
    </span>
  )
}
