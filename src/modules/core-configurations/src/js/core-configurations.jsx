import React from 'react';
import { hydrate } from 'react-dom';
import M from 'materialize-css';

import SlideSwiper from 'modules/slide-swiper-base/src/js/slide-swiper-base';
import InfoGridBase from 'modules/info-grid-base/src/js/info-grid-base';
import NavBar from 'modules/nav-bar-base/src/js/nav-bar-base';
import BannerBigPicture from 'modules/banner-big-picture/src/js/banner-big-picture';

M.AutoInit();

const config = JSON.parse(document.getElementById('config').innerHTML);
const page = JSON.parse(document.getElementById('page').innerHTML);

const theme = page.data.attributes.theme;
const currentSection = page.data.relationships.currentSection.data;
const modules = currentSection.relationships.modules.data;

modules.forEach(i => {
  if ( i.relationships.nav_bar.data !== null ) {
    const cont = i.relationships.nav_bar.data;
    const name = cont.attributes.name;
    hydrate(<NavBar config={config} page={page}/>,
      document.getElementById(name));
  }
  if ( i.relationships.slide.data !== null ) {
    const cont = i.relationships.slide.data;
    const name = cont.attributes.name;
    hydrate(<SlideSwiper config={config} slide={cont} theme={theme}/>,
      document.getElementById(name));
  }
  if ( i.relationships.info_grid.data !== null ) {
    const cont = i.relationships.info_grid.data;
    const name = cont.attributes.name;
    hydrate(<InfoGridBase config={config} theme={theme} data={cont}/>,
      document.getElementById(name));
  }
  if ( i.relationships.banner.data !== null ) {
    const cont = i.relationships.banner.data;
    const name = cont.attributes.name;
    hydrate(<BannerBigPicture config={config} theme={theme} data={cont}/>,
      document.getElementById(name));
  }
});
