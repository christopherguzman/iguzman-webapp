const fs = require('fs');
let dir = __dirname;
dir = dir.substring(0, dir.length - 10);
const filename = `${dir}/package.json`;

const dist_version = require(filename);
const a = dist_version.version.split('.');

if ( Number(a[2]) === 99 ) {
  if ( Number(a[1]) === 99 ) {
    a[0] = Number(a[0]) + 1;
    a[1] = 0;
  } else {
    a[1] = Number(a[1]) + 1;
  }
  a[2] = 0;
} else {
  a[2] = Number(a[2]) + 1;
}

dist_version.version = `${a[0]}.${a[1]}.${a[2]}`;
fs.writeFileSync(filename, JSON.stringify(dist_version, null, 2));
