import fs from 'fs';
import express from 'express';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import compress from 'compression';
import bodyParser from 'body-parser';
import hbs from 'express-hbs';
import fileStreamRotator from 'file-stream-rotator';
import axios from 'axios';
import packageJSON from '../package.json';
import { lenguageDefinition } from 'modules/common-middlewares/src/app/language-definition';

const server = express();

if ( !fs.existsSync('static') ) {
  fs.mkdirSync('static');
}

let config = require('./config/qa.json');
if ( process.env.NODE_ENV ) {
  config = require(`./config/${process.env.NODE_ENV.trim()}.json`);
}

if ( config.env.production ) {
  if (!fs.existsSync('logs')) {
    fs.mkdirSync('logs');
  }
  const accessLogStream = fileStreamRotator.getStream({
    date_format: 'YYYY-MM-DD',
    filename: './logs/%DATE%.log',
    frecuency: 'daily',
    verbose: true
  });
  server.use(morgan('common', {
    stream: accessLogStream
  }));
} else {
  server.use(morgan('dev'));
}

server.use(compress());
server.use(cookieParser());
server.use( bodyParser.json() );
server.use(bodyParser.urlencoded({ extended: true }));
server.use(function(err, req, res, next) {
  res.status(400).send({
    success: false,
    message: 'Invalid JSON',
    err: err
  });
});

server.route('/').get(( req, res ) => {
  let host = `http://${req.headers.host}`;
  if ( req.connection.encrypted ) {
    host = `https://${req.headers.host}`
  }
  let q = `${config.env.api_base}pages/?filter[dns]=${host}`;
  q += '&fields[Page]=spanish_available,english_available';
  axios.get(q)
  .then(function (response) {
    const page = response.data;
    if ( !page.data.length ) {
      return res.status(404).json({error: 'Not found'});
    }
    page.data = page.data[0];
    return lenguageDefinition(req, res, page);
  })
  .catch(function (error) {
    console.log(error);    
    return res.json(error);
  });
});

server.param('language', function( req, res, next, language ) {
  req.context.language = language;
  req.context.config.language = language;
  return next();
});

server.use(( req, res, next ) => {
  const year = new Date().getFullYear();
  req.context = {};
  req.context.config = {...config};
  delete req.context.config.env.port;
  req.context.config.year = year;
  const version = packageJSON.version;
  req.context.config.version = version;
  const fileStyles = `static/dist/styles_${version}.css`;
  if ( fs.existsSync(fileStyles) ) {
    const styles = fs.readFileSync( fileStyles, 'utf8' );
    req.context.styles = styles;
  }
  return next();
});

let viewsDir = [];
let partialsDir = [];
let isDirectory;

const pathPages = `${__dirname}/pages`;
const pages = fs.readdirSync(pathPages);
for (let i = 0; i < pages.length; i++) {
  let itemDir = `${pathPages}/${pages[i]}`;
  isDirectory = fs.lstatSync(itemDir).isDirectory();
  if ( !isDirectory ) {
    continue;
  }
  itemDir += '/src';
  if ( fs.existsSync(`${itemDir}/app`) ) {
    let appFiles = fs.readdirSync(`${itemDir}/app`);
    for (let j = 0; j < appFiles.length; j++) {
      const isRoute = appFiles[j].search(/route/g);
      if ( isRoute > -1 ) {
        const route = require(`${itemDir}/app/${appFiles[j]}`);
        route.default(server);
      }
    }
  }
  if ( fs.existsSync(`${itemDir}/assets`) ) {
    server.use( `/public/${pages[i]}`,
      express.static(`${itemDir}/assets`, { maxage: '30d' }));
  }
  if ( fs.existsSync(`${itemDir}/views`) ) {
    viewsDir.push(`${itemDir}/views`);
    if ( fs.existsSync(`${itemDir}/views/partials`) ) {
      partialsDir.push(`${itemDir}/views/partials`);
    }
  }
}

const pathModules = `${__dirname}/modules`;
const modules = fs.readdirSync(pathModules);
for (let i = 0; i < modules.length; i++) {
  let itemDir = `${pathModules}/${modules[i]}`;
  isDirectory = fs.lstatSync(itemDir).isDirectory();
  if ( !isDirectory ) {
    continue;
  }
  itemDir += '/src';
  if ( fs.existsSync(`${itemDir}/assets`) ) {
    server.use( `/public/${modules[i]}`,
      express.static(`${itemDir}/assets`, { maxage: '30d' }));
  }
  if ( fs.existsSync(`${itemDir}/views`) ) {
    partialsDir.push(`${itemDir}/views`);
  }
}

server.use( '/static', express.static('static'));
server.engine('hbs', hbs.express4({
  partialsDir: partialsDir
}));
server.set( 'views', viewsDir );
server.set( 'view engine', 'hbs' );

// Helpers
hbs.registerHelper('naturalIndex', function(index) {
  return index + 1;
});
hbs.registerHelper('equal', function(a, b, options) {
  if (arguments.length < 3) {
    throw new Error('Handlebars Helper equal needs 2 parameters');
  }
  if ( a !== b ) {
    return options.inverse(this);
  }
  return options.fn(this);
});

server.listen(config.env.port);
console.log(`Server running at http://127.0.0.1:${config.env.port}/`);

export default server;
