import {
  verifyCachePage,
  getPage,
} from 'modules/cache-handler/src/app/page-cache';
import { rebuildPage } from 'modules/common-middlewares/src/app/rebuild-page';
import { setContext } from 'modules/common-middlewares/src/app/set-context';
import { setReactElements } from 'modules/common-middlewares/src/app/set-react-elements';

/*
 *  Middleware
 */
export const index = function( req, res ) {
  return res.render( 'home-page', req.context );
};

/*
 *  Route
 */
export default function( server ) {
  server.route('/:language/')
    .get (
      verifyCachePage,
      getPage,
      rebuildPage,
      setReactElements,
      setContext,
      index
    );
}
