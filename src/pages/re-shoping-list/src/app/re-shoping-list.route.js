import {
  notForProduction
} from 'modules/common-middlewares/src/app/environment-middleware';

/*
 *  Middleware
 */
export const index = function( req, res ) {
  req.context.jsData = JSON.stringify(req.context.config);
  return res.render( 're-shoping-list', req.context );
};

/*
 *  Route
 */
export default function( server ) {
  server.get('/react/examples/shopping-list', [
    notForProduction,
    index
  ]);
}
