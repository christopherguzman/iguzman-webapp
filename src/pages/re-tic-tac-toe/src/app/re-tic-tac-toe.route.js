import React from 'react';
import ReactDOMServer from 'react-dom/server';
import {
  notForProduction
} from 'modules/common-middlewares/src/app/environment-middleware';
import TicTacToe from 'modules/tic-tac-toe/src/js/components/game';

/*
 *  Middleware
 */
export const index = function( req, res ) {
  req.context.jsData = JSON.stringify(req.context.config);
  req.context.TicTacToe = ReactDOMServer.renderToString(<TicTacToe />);
  return res.render( 're-tic-tac-toe', req.context );
};

/*
 *  Route
 */
export default function( server ) {
  server.get('/react/examples/tic-tac-toe', [
    notForProduction,
    index
  ]);
}
