# To Do

## Requirements

* Node.js
* NPM

### 1. Install dependences
```sh
$ npm i
```
### 2. Build CSS and JS
```sh
[Unix like] $ npm run build
[Win] $ npm run win-build
```
### 2. Run Server
```sh
[Unix like] $ npm run start
[Win] $ npm run win-start
```
