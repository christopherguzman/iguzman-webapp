const path = require('path');
const fs = require('fs');
const MiniCss = require('mini-css-extract-plugin');
const packageJSON = require('./package.json');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

let sourceMap = true;
if ( process.env.NODE_ENV ) {
  if ( process.env.NODE_ENV.trim() === 'master') {
    sourceMap = false;
  }
}

let jsFiles = [];
let cssFiles = [];
let isDirectory;
const pathModules = `${__dirname}/src/modules`;
const modules = fs.readdirSync(pathModules);
for (let i = 0; i < modules.length; i++) {
  let itemDir = `${pathModules}/${modules[i]}`;
  isDirectory = fs.lstatSync(itemDir).isDirectory();
  if ( !isDirectory ) {
    continue;
  }
  itemDir += '/src';
  if ( fs.existsSync(`${itemDir}/js`) ) {
    if ( fs.existsSync(`${itemDir}/js/${modules[i]}.jsx`) ) {
      jsFiles.push(`${itemDir}/js/${modules[i]}.jsx`);
    }
    if ( fs.existsSync(`${itemDir}/js/main.jsx`) ) {
      jsFiles.push(`${itemDir}/js/main.jsx`);
    }
    if ( fs.existsSync(`${itemDir}/js/index.jsx`) ) {
      jsFiles.push(`${itemDir}/js/index.jsx`);
    }
  }
  if ( fs.existsSync(`${itemDir}/scss`) ) {
    if ( fs.existsSync(`${itemDir}/scss/${modules[i]}.scss`) ) {
      cssFiles.push(`${itemDir}/scss/${modules[i]}.scss`);
    }
  }
}

const pathPages = `${__dirname}/src/pages`;
const pages = fs.readdirSync(pathPages);
for (let i = 0; i < pages.length; i++) {
  let itemDir = `${pathPages}/${pages[i]}`;
  isDirectory = fs.lstatSync(itemDir).isDirectory();
  if ( !isDirectory ) {
    continue;
  }
  itemDir += '/src';
  if ( fs.existsSync(`${itemDir}/js`) ) {
    if ( fs.existsSync(`${itemDir}/js/${pages[i]}.js`) ) {
      jsFiles.push(`${itemDir}/js/${pages[i]}.js`);
    }
    if ( fs.existsSync(`${itemDir}/js/main.js`) ) {
      jsFiles.push(`${itemDir}/js/main.js`);
    }
    if ( fs.existsSync(`${itemDir}/js/index.js`) ) {
      jsFiles.push(`${itemDir}/js/index.js`);
    }
  }
  if ( fs.existsSync(`${itemDir}/scss`) ) {
    if ( fs.existsSync(`${itemDir}/scss/${pages[i]}.scss`) ) {
      cssFiles.push(`${itemDir}/scss/${pages[i]}.scss`);
    }
  }
}

cssFiles.push(`${__dirname}/node_modules/swiper/swiper.scss`);
cssFiles.push(`${__dirname}/node_modules/materialize-css/sass/materialize.scss`);

let cssVariables = '@import "src/modules/core-configurations/src/scss/variables.scss";';
// cssVariables += '@import "node_modules/materialize-css/sass/materialize.scss";';

module.exports = {
  entry: {
    'app': jsFiles,
    'styles': cssFiles,
  },
  output: {
    filename: `[name]_${packageJSON.version}.js`,
    path: path.resolve(__dirname, 'static/dist'),
    chunkFilename: 'vendors.js'
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.css$/i,
        use: [ 'style-loader', 'css-loader', ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: MiniCss.loader
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: sourceMap,
              url: true,
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                require('autoprefixer'),
              ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: sourceMap,
              prependData: cssVariables,
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCss({
      filename: `[name]_${packageJSON.version}.css`,
      chunkFilename: 'vendors.css'
    }),
    new CompressionPlugin(),
    new LiveReloadPlugin()
  ],
  optimization: {
    splitChunks: {
      chunks: 'all',
      name: 'vendors',
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/
        }
      }
    }
  }
};
